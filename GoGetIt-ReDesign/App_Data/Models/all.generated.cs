using  System;
using  System.Collections.Generic;
using  System.Linq.Expressions;
using  System.Web;
using  Umbraco.Core.Models;
using  Umbraco.Core.Models.PublishedContent;
using  Umbraco.Web;
using  Umbraco.ModelsBuilder;
using  Umbraco.ModelsBuilder.Umbraco;
[assembly: PureLiveAssembly]
[assembly:ModelsBuilderAssembly(PureLive = true, SourceHash = "7ef0c3dfd1512d3c")]
[assembly:System.Reflection.AssemblyVersion("0.0.0.1")]


// FILE: models.generated.cs

//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.8.100
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------















namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Home Page</summary>
	[PublishedContentModel("homePage")]
	public partial class HomePage : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "homePage";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public HomePage(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<HomePage, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// About caption: This is the main content for the about section
		///</summary>
		[ImplementPropertyType("aboutCaption")]
		public IHtmlString AboutCaption
		{
			get { return this.GetPropertyValue<IHtmlString>("aboutCaption"); }
		}

		///<summary>
		/// About cta: this is the page you would like to link the button to.
		///</summary>
		[ImplementPropertyType("aboutCta")]
		public IPublishedContent AboutCta
		{
			get { return this.GetPropertyValue<IPublishedContent>("aboutCta"); }
		}

		///<summary>
		/// About image: This is the background image for the about section
		///</summary>
		[ImplementPropertyType("aboutImage")]
		public IPublishedContent AboutImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("aboutImage"); }
		}

		///<summary>
		/// About title: This is the title for the about go get it section
		///</summary>
		[ImplementPropertyType("aboutTitle")]
		public string AboutTitle
		{
			get { return this.GetPropertyValue<string>("aboutTitle"); }
		}

		///<summary>
		/// Banner Caption: This is an optional caption below the title within the banner
		///</summary>
		[ImplementPropertyType("bannerCaption")]
		public string BannerCaption
		{
			get { return this.GetPropertyValue<string>("bannerCaption"); }
		}

		///<summary>
		/// Banner image: Banner background image please optimize all images you upload with https://tinypng.com/
		///</summary>
		[ImplementPropertyType("bannerImage")]
		public IPublishedContent BannerImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("bannerImage"); }
		}

		///<summary>
		/// Banner title: Banner main title
		///</summary>
		[ImplementPropertyType("bannerTitle")]
		public string BannerTitle
		{
			get { return this.GetPropertyValue<string>("bannerTitle"); }
		}

		///<summary>
		/// Cat description: This is the description for the cat (try to keep the description length even with dog insurance and multi pet.
		///</summary>
		[ImplementPropertyType("catDescription")]
		public IHtmlString CatDescription
		{
			get { return this.GetPropertyValue<IHtmlString>("catDescription"); }
		}

		///<summary>
		/// Cat headline: This is the heading above the cat (try to keep this short enough for one line).
		///</summary>
		[ImplementPropertyType("catHeadline")]
		public string CatHeadline
		{
			get { return this.GetPropertyValue<string>("catHeadline"); }
		}

		///<summary>
		/// Dog description: This is the description for the dog(try to keep the description length even with cat insurance and multi pet.
		///</summary>
		[ImplementPropertyType("dogDescription")]
		public IHtmlString DogDescription
		{
			get { return this.GetPropertyValue<IHtmlString>("dogDescription"); }
		}

		///<summary>
		/// Dog headline: This is the heading above the dog (try to keep this short enough for one line).
		///</summary>
		[ImplementPropertyType("dogHeadline")]
		public string DogHeadline
		{
			get { return this.GetPropertyValue<string>("dogHeadline"); }
		}

		///<summary>
		/// FAQ background image: This is the background image for the FAQs section on the homepage
		///</summary>
		[ImplementPropertyType("fAQBackgroundImage")]
		public IPublishedContent FAqbackgroundImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("fAQBackgroundImage"); }
		}

		///<summary>
		/// FAQs title: This is the title for the FAQs on the home page
		///</summary>
		[ImplementPropertyType("fAQsTitle")]
		public string FAqsTitle
		{
			get { return this.GetPropertyValue<string>("fAQsTitle"); }
		}

		///<summary>
		/// Insurers title: This is the title above our insurers carousel
		///</summary>
		[ImplementPropertyType("insurersTitle")]
		public string InsurersTitle
		{
			get { return this.GetPropertyValue<string>("insurersTitle"); }
		}

		///<summary>
		/// Multipet description: This is the description for the multipet(try to keep the description length even with dog insurance and cat insurance.
		///</summary>
		[ImplementPropertyType("multipetDescription")]
		public IHtmlString MultipetDescription
		{
			get { return this.GetPropertyValue<IHtmlString>("multipetDescription"); }
		}

		///<summary>
		/// Multipet headline: This is the headline above the multi pet (try to keep this short enough for one line).
		///</summary>
		[ImplementPropertyType("multipetHeadline")]
		public string MultipetHeadline
		{
			get { return this.GetPropertyValue<string>("multipetHeadline"); }
		}

		///<summary>
		/// Slide show
		///</summary>
		[ImplementPropertyType("slideShow")]
		public IEnumerable<IPublishedContent> SlideShow
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("slideShow"); }
		}
	}

	/// <summary>Text Page</summary>
	[PublishedContentModel("textPage")]
	public partial class TextPage : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "textPage";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public TextPage(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<TextPage, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Banner caption: This is the caption within the banner
		///</summary>
		[ImplementPropertyType("bannerCaption")]
		public string BannerCaption
		{
			get { return this.GetPropertyValue<string>("bannerCaption"); }
		}

		///<summary>
		/// Banner image: This is the background image for the banner at the top of the page
		///</summary>
		[ImplementPropertyType("bannerImage")]
		public IPublishedContent BannerImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("bannerImage"); }
		}

		///<summary>
		/// Banner title: This it the title at the top of the page within the banner
		///</summary>
		[ImplementPropertyType("bannerTitle")]
		public string BannerTitle
		{
			get { return this.GetPropertyValue<string>("bannerTitle"); }
		}

		///<summary>
		/// Body text: This is the main text for the page
		///</summary>
		[ImplementPropertyType("bodyText")]
		public IHtmlString BodyText
		{
			get { return this.GetPropertyValue<IHtmlString>("bodyText"); }
		}

		///<summary>
		/// Main Title: This is the main title for the page
		///</summary>
		[ImplementPropertyType("mainTitle")]
		public string MainTitle
		{
			get { return this.GetPropertyValue<string>("mainTitle"); }
		}
	}

	/// <summary>General Settings</summary>
	[PublishedContentModel("generalSettings")]
	public partial class GeneralSettings : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "generalSettings";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public GeneralSettings(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<GeneralSettings, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Footer text
		///</summary>
		[ImplementPropertyType("footerText")]
		public IHtmlString FooterText
		{
			get { return this.GetPropertyValue<IHtmlString>("footerText"); }
		}

		///<summary>
		/// Header logo
		///</summary>
		[ImplementPropertyType("headerLogo")]
		public IPublishedContent HeaderLogo
		{
			get { return this.GetPropertyValue<IPublishedContent>("headerLogo"); }
		}

		///<summary>
		/// hide page: Hide page from global navigation
		///</summary>
		[ImplementPropertyType("umbracoNaviHide")]
		public bool UmbracoNaviHide
		{
			get { return this.GetPropertyValue<bool>("umbracoNaviHide"); }
		}
	}

	/// <summary>Slide</summary>
	[PublishedContentModel("slide")]
	public partial class Slide : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "slide";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Slide(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Slide, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// image
		///</summary>
		[ImplementPropertyType("image")]
		public IPublishedContent Image
		{
			get { return this.GetPropertyValue<IPublishedContent>("image"); }
		}
	}

	/// <summary>News</summary>
	[PublishedContentModel("news")]
	public partial class News : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "news";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public News(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<News, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Banner caption: This is the banner caption for the news page
		///</summary>
		[ImplementPropertyType("bannerCaption")]
		public string BannerCaption
		{
			get { return this.GetPropertyValue<string>("bannerCaption"); }
		}

		///<summary>
		/// Banner image: Background image for the news gateway page
		///</summary>
		[ImplementPropertyType("bannerImage")]
		public IPublishedContent BannerImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("bannerImage"); }
		}

		///<summary>
		/// Banner title: Title on top of the main banner for the news section
		///</summary>
		[ImplementPropertyType("bannerTitle")]
		public string BannerTitle
		{
			get { return this.GetPropertyValue<string>("bannerTitle"); }
		}
	}

	/// <summary>News Story</summary>
	[PublishedContentModel("newsPage")]
	public partial class NewsPage : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "newsPage";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public NewsPage(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<NewsPage, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Main content
		///</summary>
		[ImplementPropertyType("mainContent")]
		public IHtmlString MainContent
		{
			get { return this.GetPropertyValue<IHtmlString>("mainContent"); }
		}

		///<summary>
		/// Main title: This is the title of the news article
		///</summary>
		[ImplementPropertyType("mainTitle")]
		public string MainTitle
		{
			get { return this.GetPropertyValue<string>("mainTitle"); }
		}

		///<summary>
		/// Thumbnail date: This is the date you published the news article DD.MM.YYYY
		///</summary>
		[ImplementPropertyType("thumbnailDate")]
		public string ThumbnailDate
		{
			get { return this.GetPropertyValue<string>("thumbnailDate"); }
		}

		///<summary>
		/// Thumbnail description: This is the short description for the news article card (this should be short and of a similar length to other news card descriptions).
		///</summary>
		[ImplementPropertyType("thumbnailDescription")]
		public IHtmlString ThumbnailDescription
		{
			get { return this.GetPropertyValue<IHtmlString>("thumbnailDescription"); }
		}

		///<summary>
		/// Thumbnail picture: This is the picture that will appear in the news story card
		///</summary>
		[ImplementPropertyType("thumbnailPicture")]
		public IPublishedContent ThumbnailPicture
		{
			get { return this.GetPropertyValue<IPublishedContent>("thumbnailPicture"); }
		}

		///<summary>
		/// Thumbnail title: This is the title for the news article card
		///</summary>
		[ImplementPropertyType("thumbnailTitle")]
		public string ThumbnailTitle
		{
			get { return this.GetPropertyValue<string>("thumbnailTitle"); }
		}
	}

	/// <summary>FAQ items</summary>
	[PublishedContentModel("fAQ")]
	public partial class FAQ : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "fAQ";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public FAQ(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<FAQ, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Answer: This is the answer for the FAQ
		///</summary>
		[ImplementPropertyType("answer")]
		public IHtmlString Answer
		{
			get { return this.GetPropertyValue<IHtmlString>("answer"); }
		}

		///<summary>
		/// Question: This is the question for the FAQ
		///</summary>
		[ImplementPropertyType("question")]
		public string Question
		{
			get { return this.GetPropertyValue<string>("question"); }
		}
	}

	/// <summary>FAQ</summary>
	[PublishedContentModel("fAQItem")]
	public partial class FAqitem : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "fAQItem";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public FAqitem(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<FAqitem, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Banner caption: This is the caption that sits within the main banner
		///</summary>
		[ImplementPropertyType("bannerCaption")]
		public string BannerCaption
		{
			get { return this.GetPropertyValue<string>("bannerCaption"); }
		}

		///<summary>
		/// Banner image: This is the background image for the banner at the top of the page
		///</summary>
		[ImplementPropertyType("bannerImage")]
		public IPublishedContent BannerImage
		{
			get { return this.GetPropertyValue<IPublishedContent>("bannerImage"); }
		}

		///<summary>
		/// Banner title: This is the main title for the page that sits on top of the banner
		///</summary>
		[ImplementPropertyType("bannerTitle")]
		public string BannerTitle
		{
			get { return this.GetPropertyValue<string>("bannerTitle"); }
		}

		///<summary>
		/// Faqs: These are the FAQ items
		///</summary>
		[ImplementPropertyType("faqs")]
		public IEnumerable<IPublishedContent> Faqs
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("faqs"); }
		}
	}

	/// <summary>Folder</summary>
	[PublishedContentModel("Folder")]
	public partial class Folder : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "Folder";
		public new const PublishedItemType ModelItemType = PublishedItemType.Media;
#pragma warning restore 0109

		public Folder(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Folder, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Contents:
		///</summary>
		[ImplementPropertyType("contents")]
		public object Contents
		{
			get { return this.GetPropertyValue("contents"); }
		}
	}

	/// <summary>Image</summary>
	[PublishedContentModel("Image")]
	public partial class Image : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "Image";
		public new const PublishedItemType ModelItemType = PublishedItemType.Media;
#pragma warning restore 0109

		public Image(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Image, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Size
		///</summary>
		[ImplementPropertyType("umbracoBytes")]
		public string UmbracoBytes
		{
			get { return this.GetPropertyValue<string>("umbracoBytes"); }
		}

		///<summary>
		/// Type
		///</summary>
		[ImplementPropertyType("umbracoExtension")]
		public string UmbracoExtension
		{
			get { return this.GetPropertyValue<string>("umbracoExtension"); }
		}

		///<summary>
		/// Upload image
		///</summary>
		[ImplementPropertyType("umbracoFile")]
		public Umbraco.Web.Models.ImageCropDataSet UmbracoFile
		{
			get { return this.GetPropertyValue<Umbraco.Web.Models.ImageCropDataSet>("umbracoFile"); }
		}

		///<summary>
		/// Height
		///</summary>
		[ImplementPropertyType("umbracoHeight")]
		public string UmbracoHeight
		{
			get { return this.GetPropertyValue<string>("umbracoHeight"); }
		}

		///<summary>
		/// Width
		///</summary>
		[ImplementPropertyType("umbracoWidth")]
		public string UmbracoWidth
		{
			get { return this.GetPropertyValue<string>("umbracoWidth"); }
		}
	}

	/// <summary>File</summary>
	[PublishedContentModel("File")]
	public partial class File : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "File";
		public new const PublishedItemType ModelItemType = PublishedItemType.Media;
#pragma warning restore 0109

		public File(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<File, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Size
		///</summary>
		[ImplementPropertyType("umbracoBytes")]
		public string UmbracoBytes
		{
			get { return this.GetPropertyValue<string>("umbracoBytes"); }
		}

		///<summary>
		/// Type
		///</summary>
		[ImplementPropertyType("umbracoExtension")]
		public string UmbracoExtension
		{
			get { return this.GetPropertyValue<string>("umbracoExtension"); }
		}

		///<summary>
		/// Upload file
		///</summary>
		[ImplementPropertyType("umbracoFile")]
		public string UmbracoFile
		{
			get { return this.GetPropertyValue<string>("umbracoFile"); }
		}
	}

	/// <summary>Member</summary>
	[PublishedContentModel("Member")]
	public partial class Member : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "Member";
		public new const PublishedItemType ModelItemType = PublishedItemType.Member;
#pragma warning restore 0109

		public Member(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Member, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Is Approved
		///</summary>
		[ImplementPropertyType("umbracoMemberApproved")]
		public bool UmbracoMemberApproved
		{
			get { return this.GetPropertyValue<bool>("umbracoMemberApproved"); }
		}

		///<summary>
		/// Comments
		///</summary>
		[ImplementPropertyType("umbracoMemberComments")]
		public string UmbracoMemberComments
		{
			get { return this.GetPropertyValue<string>("umbracoMemberComments"); }
		}

		///<summary>
		/// Failed Password Attempts
		///</summary>
		[ImplementPropertyType("umbracoMemberFailedPasswordAttempts")]
		public string UmbracoMemberFailedPasswordAttempts
		{
			get { return this.GetPropertyValue<string>("umbracoMemberFailedPasswordAttempts"); }
		}

		///<summary>
		/// Last Lockout Date
		///</summary>
		[ImplementPropertyType("umbracoMemberLastLockoutDate")]
		public string UmbracoMemberLastLockoutDate
		{
			get { return this.GetPropertyValue<string>("umbracoMemberLastLockoutDate"); }
		}

		///<summary>
		/// Last Login Date
		///</summary>
		[ImplementPropertyType("umbracoMemberLastLogin")]
		public string UmbracoMemberLastLogin
		{
			get { return this.GetPropertyValue<string>("umbracoMemberLastLogin"); }
		}

		///<summary>
		/// Last Password Change Date
		///</summary>
		[ImplementPropertyType("umbracoMemberLastPasswordChangeDate")]
		public string UmbracoMemberLastPasswordChangeDate
		{
			get { return this.GetPropertyValue<string>("umbracoMemberLastPasswordChangeDate"); }
		}

		///<summary>
		/// Is Locked Out
		///</summary>
		[ImplementPropertyType("umbracoMemberLockedOut")]
		public bool UmbracoMemberLockedOut
		{
			get { return this.GetPropertyValue<bool>("umbracoMemberLockedOut"); }
		}

		///<summary>
		/// Password Answer
		///</summary>
		[ImplementPropertyType("umbracoMemberPasswordRetrievalAnswer")]
		public string UmbracoMemberPasswordRetrievalAnswer
		{
			get { return this.GetPropertyValue<string>("umbracoMemberPasswordRetrievalAnswer"); }
		}

		///<summary>
		/// Password Question
		///</summary>
		[ImplementPropertyType("umbracoMemberPasswordRetrievalQuestion")]
		public string UmbracoMemberPasswordRetrievalQuestion
		{
			get { return this.GetPropertyValue<string>("umbracoMemberPasswordRetrievalQuestion"); }
		}
	}

}



// EOF
