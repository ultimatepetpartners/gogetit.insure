﻿/* ===========================================================

	# GLOBAL JS

=========================================================== */

var changeHeight = function () {
    headerHeight = $('.m-header').height();
    $('.o-page').css('padding-top', headerHeight);
};

$(function () {
    $('.c-faq__question').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('js-button');
        $(this).next('.c-faq__answer').slideToggle();

    });

    $('.c-carousel--insurers').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        responsive: [
          {
              breakpoint: 1024,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
              }
          },
          {
              breakpoint: 600,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
        ]
    });

    $('.m-header').addClass('js-header__fixed');

    changeHeight();
    $(window).on('resize', function () {
        changeHeight();
    });

    var scrollDelta = 300;
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();

        if (scroll >= scrollDelta) {
            $('.m-header').addClass('js-header__shrink');
        } else {
            $('.m-header').removeClass('js-header__shrink');
        }
    });
});